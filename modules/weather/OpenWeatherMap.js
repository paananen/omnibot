var http = require("http");
var urlHelper = require("../../../TelegramBot/url-helper");

function OpenWeatherMap(token) {
	this.cityID = 655195;
	this.token = token;
	this.httpOptions = {
		hostname : "api.openweathermap.org",
		path : "/data/2.5/weather?id=" + this.cityID + "&APPID=" + this.token
	}
}

OpenWeatherMap.prototype.getWeather = function (responseHandler) {
	var request = http.request(this.httpOptions, function (response) {
		response.on("data", function (data) {
			var weather = JSON.parse(data);
			responseHandler(weather);
		});
	});
	request.end();
}

module.exports = OpenWeatherMap;