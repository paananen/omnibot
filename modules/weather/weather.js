var fs = require('fs');
var OpenWeatherMap = require('./OpenWeatherMap');

function WeatherModule(parent) {
	this.parent = parent;
	var that = this;
	this.weather = {};
	fs.readFile('modules/weather/open-weather-map-token.txt', function (err, data) {
		if(err) {
			console.log(err);
		} else {
			that.token = data;
		}
		that.init();
	});
}

WeatherModule.prototype.init = function () {
	this.weatherAPI = new OpenWeatherMap(this.token);
	this.updateWeather();
	setInterval(this.updateWeather.bind(this), 600000);
	this.parent.registerCommand('sää', this.handleCommand, this);
}

WeatherModule.prototype.updateWeather = function () {
	this.weatherAPI.getWeather(this.handleWeather.bind(this));
}

WeatherModule.prototype.handleWeather = function (response) {
	this.weather = {
		temperature : response.main.temp - 272.15,
		code : response.weather[0].id,
	};
	console.log(this.weather);
}

WeatherModule.prototype.handleCommand = function (command, options, chatID) {
	switch(command) {
		case 'sää':
			var weatherType;
			switch(this.weather.code) {
				case 200:
				case 201:
				case 202:
				case 210:
				case 211:
				case 212:
				case 221:
				case 230:
				case 231:
				case 232:
					weatherType = "ukkosmyrsky";
					break;
				case 300:
				case 301:
				case 302:
				case 310:
				case 311:
				case 312:
				case 313:
				case 314:
				case 321:
				case 500:
				case 501:
				case 502:
				case 503:
				case 504:
				case 511:
				case 520:
				case 521:
				case 522:
				case 531:
					weatherType = "sataa vettä";
					break;
				case 600:
				case 601:
				case 602:
				case 620:
				case 621:
				case 622:
					weatherType = "sataa lunta";
					break;
				case 611:
				case 612:
				case 615:
				case 616:
					weatherType = "sataa räntää tai jotain töhnää";
					break;
				case 701:
				case 711:
				case 721:
				case 731:
				case 741:
				case 751:
				case 761:
				case 762:
				case 771:
				case 781:
					weatherType = "ilmassa on jotain";
					break;
				case 800:
				case 801:
				case 802:
				case 803:
					weatherType = "poutaa";
					shouldDrinkInside = false;
					break;
				case 804:
					weatherType = "pilvistä";
					shouldDrinkInside = false;
					break;
				case 900:
				case 901:
				case 902:
				case 903:
				case 904:
				case 905:
				case 906:
					weatherType = "sään jumalat haluaa tappaa jonkun";
					break;
				case 951:
				case 952:
				case 953:
					weatherType = "tyyntä";
					shouldDrinkInside = false;
					break;
				case 954:
				case 955:
				case 956:
				case 957:
				case 958:
				case 959:
				case 960:
				case 961:
				case 962:
					weatherType = "tuulista";
					break;
			}
			var temperature = Math.round(this.weather.temperature);
			this.parent.sendMessage('Siellä on ' + Math.abs(temperature) + ' aste' + (Math.abs(temperature) != 1 ? 'tta' : '') + ' ' + (temperature < 0 ? 'pakkasta' : 'lämmintä') + ' ja ' + weatherType + '.', chatID);
			console.log(this.weather);
			break;
	}
};

module.exports = WeatherModule;