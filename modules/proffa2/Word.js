function Word(text, predecessors, successors) {
	this.text = text;
	this.predecessors = typeof predecessors != 'undefined' ? predecessors : [];
	this.successors = typeof successors != 'undefined' ? successors : [];
	this.predecessorInstances = this.countInstances(this.predecessors);
	this.successorInstances = this.countInstances(this.successors);
}

Word.prototype.countInstances = function (input) {
	var total = 0;
	for(var i = 0; i < input.length; i++) {
		total += input[i].instances;
	}
	
	return total;
}

Word.prototype.getPredecessor = function () {
	var place = Math.floor(Math.random() * this.predecessorInstances);
	var progress = 0;
	for(var i = 0; i < this.predecessors.length; i++) {
		if(progress + this.predecessors[i].instances > place) {
			return this.predecessors[i].text;
		}
		progress += this.predecessors[i].instances;
	}
}

Word.prototype.getSuccessor = function () {
	var place = Math.floor(Math.random() * this.successorInstances);
	var progress = 0;
	for(var i = 0; i < this.successors.length; i++) {
		if(progress + this.successors[i].instances > place) {
			return this.successors[i].text;
		}
		progress += this.successors[i].instances;
	}
}

Word.prototype.addPredecessorInstance = function (text) {
	if(typeof text == 'undefined') {
		return;
	}
	
	for(var i = 0; i < this.predecessors.length; i++) {
		if(this.predecessors[i].text == text) {
			this.incrementPredecessor(this.predecessors[i]);
			return;
		}
	}
	this.addPredecessor(text);
}

Word.prototype.addSuccessorInstance = function (text) {
	if(typeof text == 'undefined') {
		return;
	}
	
	for(var i = 0; i < this.successors.length; i++) {
		if(this.successors[i].text == text) {
			this.incrementSuccessor(this.successors[i]);
			return;
		}
	}
	this.addSuccessor(text);
}

Word.prototype.incrementPredecessor = function (predecessor) {
	this.predecessorInstances++;
	predecessor.instances++;
}

Word.prototype.incrementSuccessor = function (successor) {
	this.successorInstances++;
	successor.instances++;
}

Word.prototype.addPredecessor = function (text) {
	this.predecessors.push({
		text: text,
		instances: 1
	});
	this.predecessorInstances++;
}

Word.prototype.addSuccessor = function (text) {
	this.successors.push({
		text: text,
		instances: 1
	});
	this.successorInstances++;
}

module.exports = Word;