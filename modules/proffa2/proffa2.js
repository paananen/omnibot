var fs = require('fs');
var Word = require('./Word.js');

const MILLISECOND = 1;
const SECOND = 1000 * MILLISECOND;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;

function Proffa2Module(parent) {
	this.parent = parent;
	this.filename = 'elamam_koulu_proffa2.json';
	this.wordBlacklist = [
		'proffa'
	];
	this.words = [];
	this.loadFile(this.filename);
	this.saveInterval = 5 * SECOND;
	this.messageLog = [];
	this.ownMessageLog = [];
	setInterval(this.removeOldMessagesFromLog.bind(this), 1 * HOUR);
}

Proffa2Module.prototype.loadFile = function (filename) {
	var that = this;
	fs.readFile(filename, function (err, data) {
		if(err) {
			console.log(err);
		} else {
			if(data.length > 0) {
				var tempData = JSON.parse(data);
				for(var i = 0; i < tempData.length; i++) {
					that.words.push(new Word(tempData[i].text, tempData[i].predecessors, tempData[i].successors));
				}
			}
			that.parent.on('message', that.handleMessage, that);
			that.parent.registerCommand('test', that.handleTestCommand, that);
		}
	});
}

Proffa2Module.prototype.saveFile = function (filename) {
	if(this.saveAllowed == false) {
		if(global.debug) console.log('Save deferred');
		return;
	}
	
	fs.writeFile(filename, JSON.stringify(this.words), function (err) {
		if(err) {
			console.log(err);
		} else {
			if(global.debug) console.log(filename + ' saved');
		}
	});
	
	this.saveAllowed = false;
	setInterval((function () {
		this.saveAllowed = true;
	}).bind(this), this.saveInterval);
}

Proffa2Module.prototype.handleMessage = function (message) {
	if(message.chat.id != -2794222 && !global.debug) {
		return;
	}

	if(message.from.is_bot) {
		return;
	}

	if(message.text.indexOf('/') === 0) {
		return;
	}
	
	if(message.text.toLowerCase().indexOf('proffa') > -1) {
		this.respond(message, true);
	}
	
	for(var i = 0; i < this.wordBlacklist.length; i++) {
		if(message.text.toLowerCase().indexOf(this.wordBlacklist[i].toLowerCase()) > -1) {
			return;
		}
	}

	this.logMessage(message);

	if(!global.debug) this.learnMessage(message);

	this.possiblyRespond(message);
}

Proffa2Module.prototype.sendMessage = function (messageText, chatId) {
	this.ownMessageLog.push({
		date: Date.now()
	});

	this.parent.sendMessage(messageText, chatId);
}

Proffa2Module.prototype.respond = function (message, forceAnswer = false) {
	var responseText = 'En ymmärrä mistä puhut.';
	var contextWord = this.findMostMeaningfulWordInText(message.text);
	if(global.debug) console.log('context word: ', contextWord);
	if(contextWord !== false) {
		responseText = this.buildMessageFromWord(contextWord);
	} else if(!forceAnswer) {
		return false;
	}
	
	this.sendMessage(responseText, message.chat.id);
}

Proffa2Module.prototype.possiblyRespond = function (message) {
	let currentTimestamp = Date.now();
	let recentMessages = this.messageLog.filter(message => {
		return currentTimestamp - message.date < 10 * MINUTE;
	});
	let recentMessageCount = recentMessages.length;
	let answerProbability = recentMessageCount / (recentMessageCount + 100) / 10;

	let recentOwnMessages = this.ownMessageLog.filter(message => {
		return currentTimestamp - message.date < 1 * MINUTE;
	});

	if(recentOwnMessages.length > 0) {
		answerProbability *= 50;
	}

	if(global.debug) console.log('Answer probability: ', answerProbability);

	if(Math.random() < answerProbability) {
		if(global.debug) console.log('I decided to answer');
		this.respond(message);
	}
}

Proffa2Module.prototype.logMessage = function (message) {
	message.date *= SECOND; // Convert unix timestamp to js timestamp (s to ms)
	this.messageLog.push(message);
}

Proffa2Module.prototype.logOwnMessage = function (message) {
	this.ownMessageLog.push(message);
}

Proffa2Module.prototype.removeOldMessagesFromLog = function () {
	for(let i = 0; i < this.messageLog.length; i++) {
		let message = this.messageLog[i];
		let currentTimestamp = Date.now();
		if(currentTimestamp - message.date > 24 * HOUR) {
			this.messageLog.splice(i, 1);
		}
	}

	for(let i = 0; i < this.ownMessageLog.length; i++) {
		let message = this.ownMessageLog[i];
		let currentTimestamp = Date.now();
		if(currentTimestamp - message.date > 24 * HOUR) {
			this.ownMessageLog.splice(i, 1);
		}
	}
}

Proffa2Module.prototype.learnMessage = function (message) {
	var words = message.text.split(' ');
	words.unshift(null);
	words.push(null);
	if(global.debug) console.log(words);
	for(var i = 0; i < words.length; i++) {
		this.learnWord(words[i], words[i-1], words[i+1]);
	}
	// if(global.debug) console.log(JSON.stringify(this.words));
	this.saveFile(this.filename);
}

Proffa2Module.prototype.handleTestCommand = function (command, options, chatID) {
	var messageText = this.buildMessageFromWord(options[0]);
	if(messageText === false) {
		messageText = 'En tiedä mitään aiheesta "' + options[0] + '"';
	}
	this.sendMessage(messageText, chatID);
}

Proffa2Module.prototype.buildMessageFromWord = function (wordText) {
	var word = this.findWord(wordText);
	if(word === false) {
		return false;
	}
	
	var words = [word];
	
	while(true) {
		var predecessor = this.findWord(words[0].getPredecessor());
		if(predecessor.text == null) {
			break;
		}
		
		words.unshift(predecessor);
	}
	
	while(true) {
		var successor = this.findWord(words[words.length - 1].getSuccessor());
		if(successor.text == null) {
			break;
		}
		
		words.push(successor);
	}
	
	var messageText = words.map(word => word.text).join(' ');
	return messageText;
}

Proffa2Module.prototype.findWord = function (text) {
	for(var i = 0; i < this.words.length; i++) {
		if(this.words[i].text == text) {
			return this.words[i];
		}
	}
	
	return false;
}

Proffa2Module.prototype.findMostMeaningfulWordInText = function (text) {
	var words = text.split(' ');
	var lowestI, lowestConnections;
	for(var i = 0; i < words.length; i++) {
		var word = this.findWord(words[i]);
		if(word === false) {
			continue;
		}
		var connections = word.predecessorInstances + word.successorInstances;
		if(connections < lowestConnections || typeof lowestConnections == 'undefined') {
			lowestI = i;
			lowestConnections = connections;
		}
	}
	
	if(typeof lowestI == 'undefined') {
		return false;
	}
	
	return words[lowestI];
}

Proffa2Module.prototype.learnWord = function (text, predecessorText, successorText) {
	for(var i = 0; i < this.words.length; i++) {
		if(this.words[i].text == text) {
			this.words[i].addPredecessorInstance(predecessorText);
			this.words[i].addSuccessorInstance(successorText);
			return;
		}
	}
	
	this.words.push(new Word(text));
	this.learnWord(text, predecessorText, successorText);
}

module.exports = Proffa2Module;
