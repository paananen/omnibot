var Learner = require('../../../random-text/Learner');
var Generator = require('../../../random-text/Generator');

function ProffaModule(parent) {
	var that = this;
	this.parent = parent;
	this.parent.on('message', this.handleMessage, this);
	this.parent.registerCommand('letters', this.handleTestCommand, this);
	this.pseudoFinnishGenerator;
	this.pseudoFinnishLearner = new Learner({
		order: 3,
		filename: 'finnish-letters-3',
		type: Learner.LETTERS_LEARNER
	}, function () {
		that.pseudoFinnishGenerator = new Generator({chain: that.pseudoFinnishLearner.chain, type: Generator.LETTERS_GENERATOR});
	});
	
	this.chainSaveInterval = setInterval(function () {
		that.pseudoFinnishLearner.chain.saveFile();
	}, 10000);
	
	this.wordBlacklist = [
		'/test',
		'proffa'
	];
	
	this.activationWords = [
		'proffa'
	];
}

ProffaModule.prototype.handleMessage = function (message) {
	for(var i = 0; i < this.activationWords.length; i++) {
		if(message.text.indexOf(this.activationWords[i]) > -1) {
			this.respond(message);
		}
	}
	for(var i = 0; i < this.wordBlacklist.length; i++) {
		if(message.text.indexOf(this.wordBlacklist[i]) > -1 || message.text.indexOf('/') === 0) {
			return;
		}
	}
	this.pseudoFinnishLearner.learn(message.text);
};

ProffaModule.prototype.handleTestCommand = function (command, options, chatID) {
	var messageText = this.pseudoFinnishGenerator.generateString();
	this.parent.sendMessage(messageText, chatID);
};

ProffaModule.prototype.respond = function (message) {
	return false;
	var messageText = this.pseudoFinnishGenerator.generateString();
	this.parent.sendMessage(messageText, message.chat.id);
}

module.exports = ProffaModule;