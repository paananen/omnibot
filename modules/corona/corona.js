var fs = require('fs');
var https = require('https');

function CoronaModule(parent) {
	this.parent = parent;
	this.parent.registerCommand('korona', this.handleCoronaCommand, this);
	this.parent.registerCommand('start', this.handleStart, this);
	this.filename = 'corona_data.json';
	this.data = {confirmed: [], deaths: [], chats: []};
	this.newData = {confirmed: [], deaths: []};
	this.loadFile(this.filename, this.init.bind(this));
	this.activationWords = ['yes', 'on', 'kyllä', '1', 'true'];
	this.deactivationWords = ['no', 'off', 'ei', 'stop', 'lopeta', 'seis', '0', 'false'];
	this.subscribeMessages = ['Aye aye, sir.', 'Koronapäivityksiä luvassa.', 'Selvä.', 'Kuluupahan F5 vähemmän.'];
	this.unsubscribeMessages = ['🤐', 'Ei sitten.', 'Näkeehän ne Iltalehdestäkin.'];
	this.confusionMessages = ['🤨', '🤔'];
	this.INFECTION_SOURCE_UNKOWN = 'unknown';
	this.INFECTION_SOURCE_RELATED = 'related to earlier';
	this.CASE_TYPE_CONFIRMED = 'confirmed';
	this.CASE_TYPE_DEATHS = 'deaths';
	this.CASE_TYPES = [this.CASE_TYPE_CONFIRMED, this.CASE_TYPE_DEATHS];
	this.GRAMMATICAL_CASE_NOMINATIVE = 'nominative';
	this.GRAMMATICAL_CASE_ELATIVE = 'elative';
	this.GRAMMATICAL_CASE_GENITIVE = 'genitive';
	this.countries = {[this.GRAMMATICAL_CASE_NOMINATIVE]: {AFG: 'Afganistan', ALA: 'Ahvenanmaa', NLD: 'Alankomaat', ANT: 'Alankomaiden Antillit', ALB: 'Albania', DZA: 'Algeria', ASM: 'Amerikan Samoa', AND: 'Andorra', AGO: 'Angola', AIA: 'Anguilla', ATA: 'Antarktis', ATG: 'Antigua ja Barbuda', ARE: 'Arabiemiirikunnat', ARG: 'Argentiina', ARM: 'Armenia', ABW: 'Aruba', AUS: 'Australia', AZE: 'Azerbaidžan', BHS: 'Bahama', BHR: 'Bahrain', BGD: 'Bangladesh', BRB: 'Barbados', BEL: 'Belgia', BLZ: 'Belize', BEN: 'Benin', BMU: 'Bermuda', BTN: 'Bhutan', BOL: 'Bolivia', BIH: 'Bosnia ja Hertsegovina', BWA: 'Botswana', BVT: 'Bouvet’nsaari', BRA: 'Brasilia', IOT: 'Brittiläinen Intian valtameren alue', VGB: 'Brittiläiset Neitsytsaaret', BRN: 'Brunei', BGR: 'Bulgaria', BFA: 'Burkina Faso', BDI: 'Burundi', CYM: 'Caymansaaret', CHL: 'Chile', COK: 'Cookinsaaret', CRI: 'Costa Rica', DJI: 'Djibouti', DMA: 'Dominica', DOM: 'Dominikaaninen tasavalta', ECU: 'Ecuador', EGY: 'Egypti', SLV: 'El Salvador', ERI: 'Eritrea', ESP: 'Espanja', ETH: 'Etiopia', ZAF: 'Etelä-Afrikka', SGS: 'Etelä-Georgia ja Eteläiset Sandwichsaaret', FLK: 'Falklandinsaaret', FRO: 'Färsaaret', FJI: 'Fidži', PHL: 'Filippiinit', GAB: 'Gabon', GMB: 'Gambia', GEO: 'Georgia', GHA: 'Ghana', GIB: 'Gibraltar', GRD: 'Grenada', GRL: 'Grönlanti', GLP: 'Guadeloupe', GUM: 'Guam', GTM: 'Guatemala', GGY: 'Guernsey', GIN: 'Guinea', GNB: 'Guinea-Bissau', GUY: 'Guyana', HTI: 'Haiti', HMD: 'Heard ja McDonaldinsaaret', HND: 'Honduras', HKG: 'Hongkong', IDN: 'Indonesia', IND: 'Intia', IRQ: 'Irak', IRN: 'Iran', IRL: 'Irlanti', ISL: 'Islanti', ISR: 'Israel', ITA: 'Italia', TLS: 'Itä-Timor', AUT: 'Itävalta', JAM: 'Jamaika', JPN: 'Japani', YEM: 'Jemen', JEY: 'Jersey', JOR: 'Jordania', CXR: 'Joulusaari', KHM: 'Kambodža', CMR: 'Kamerun', CAN: 'Kanada', CPV: 'Kap Verde', KAZ: 'Kazakstan', KEN: 'Kenia', CAF: 'Keski-Afrikan tasavalta', CHN: 'Kiina', KGZ: 'Kirgisia', KIR: 'Kiribati', COL: 'Kolumbia', COM: 'Komorit', COD: 'Kongon demokraattinen tasavalta', COG: 'Kongon tasavalta', CCK: 'Kookossaaret', PRK: 'Korean demokraattinen kansantasavalta', KOR: 'Korean tasavalta', GRC: 'Kreikka', HRV: 'Kroatia', CUB: 'Kuuba', KWT: 'Kuwait', CYP: 'Kypros', LAO: 'Laos', LVA: 'Latvia', LSO: 'Lesotho', LBN: 'Libanon', LBR: 'Liberia', LBY: 'Libya', LIE: 'Liechtenstein', LTU: 'Liettua', LUX: 'Luxemburg', ESH: 'Länsi-Sahara', MAC: 'Macao', MDG: 'Madagaskar', MWI: 'Malawi', MDV: 'Malediivit', MYS: 'Malesia', MLI: 'Mali', MLT: 'Malta', IMN: 'Mansaari', MAR: 'Marokko', MHL: 'Marshallinsaaret', MTQ: 'Martinique', MRT: 'Mauritania', MUS: 'Mauritius', MYT: 'Mayotte', MEX: 'Meksiko', FSM: 'Mikronesian liittovaltio', MDA: 'Moldova', MCO: 'Monaco', MNG: 'Mongolia', MNE: 'Montenegro', MSR: 'Montserrat', MOZ: 'Mosambik', MMR: 'Myanmar', NAM: 'Namibia', NRU: 'Nauru', NPL: 'Nepal', NIC: 'Nicaragua', NER: 'Niger', NGA: 'Nigeria', NIU: 'Niue', NFK: 'Norfolkinsaari', NOR: 'Norja', CIV: 'Norsunluurannikko', OMN: 'Oman', PAK: 'Pakistan', PLW: 'Palau', PSE: 'Palestiina', PAN: 'Panama', PNG: 'Papua-Uusi-Guinea', PRY: 'Paraguay', PER: 'Peru', MNP: 'Pohjois-Mariaanit', PCN: 'Pitcairn', MKD: 'Pohjois-Makedonia', PRT: 'Portugali', PRI: 'Puerto Rico', POL: 'Puola', GNQ: 'Päiväntasaajan Guinea', QAT: 'Qatar', FRA: 'Ranska', ATF: 'Ranskan eteläiset alueet', GUF: 'Ranskan Guayana', PYF: 'Ranskan Polynesia', REU: 'Réunion', ROU: 'Romania', RWA: 'Ruanda', SWE: 'Ruotsi', SHN: 'Saint Helena', KNA: 'Saint Kitts ja Nevis', LCA: 'Saint Lucia', SPM: 'Saint-Pierre ja Miquelon', VCT: 'Saint Vincent ja Grenadiinit', DEU: 'Saksa', SLB: 'Salomonsaaret', ZMB: 'Sambia', WSM: 'Samoa', SMR: 'San Marino', STP: 'São Tomé ja Príncipe', SAU: 'Saudi-Arabia', SEN: 'Senegal', SRB: 'Serbia', SYC: 'Seychellit', SLE: 'Sierra Leone', SGP: 'Singapore', SVK: 'Slovakia', SVN: 'Slovenia', SOM: 'Somalia', LKA: 'Sri Lanka', SDN: 'Sudan', FIN: 'Suomi', SUR: 'Suriname', SJM: 'Svalbard ja Jan Mayen', SWZ: 'Swazimaa', CHE: 'Sveitsi', SYR: 'Syyria', TJK: 'Tadžikistan', TWN: 'Taiwan', TZA: 'Tansania', DNK: 'Tanska', THA: 'Thaimaa', TGO: 'Togo', TKL: 'Tokelau', TON: 'Tonga', TTO: 'Trinidad ja Tobago', TCD: 'Tšad', CZE: 'Tšekki', TUN: 'Tunisia', TUR: 'Turkki', TKM: 'Turkmenistan', TCA: 'Turks- ja Caicossaaret', TUV: 'Tuvalu', UGA: 'Uganda', UKR: 'Ukraina', HUN: 'Unkari', URY: 'Uruguay', NCL: 'Uusi-Kaledonia', NZL: 'Uusi-Seelanti', UZB: 'Uzbekistan', BLR: 'Valko-Venäjä', VUT: 'Vanuatu', VAT: 'Vatikaanivaltio', VEN: 'Venezuela', RUS: 'Venäjä', VNM: 'Vietnam', EST: 'Viro', WLF: 'Wallis ja Futunasaaret', GBR: 'Yhdistynyt kuningaskunta', USA: 'Yhdysvallat', VIR: 'Yhdysvaltain Neitsytsaaret', UMI: 'Yhdysvaltain pienet erillissaaret', ZWE: 'Zimbabwe'}, [this.GRAMMATICAL_CASE_ELATIVE]: {ITA: 'Italiasta', FIN: 'Suomesta', CHN: 'Kiinasta', AUS: 'Australiasta', IRN: 'Iranista'}};
	this.healthCareDistricts = {
		[this.GRAMMATICAL_CASE_GENITIVE]: {
			'Etelä-Karjala': 'Etelä-Karjalan',
			'Etelä-Pohjanmaa': 'Etelä-Pohjanmaan',
			'Etelä-Savo': 'Etelä-Savon',
			'HUS': 'Helsingin ja Uudenmaan',
			'Itä-Savo': 'Itä-Savon',
			'Kainuu': 'Kainuun',
			'Kanta-Häme': 'Kanta-Hämeen',
			'Keski-Pohjanmaa': 'Keski-Pohjanmaan',
			'Keski-Suomi': 'Keski-Suomen',
			'Kymenlaakso': 'Kymenlaakson',
			'Lappi': 'Lapin',
			'Länsi-Pohja': 'Länsi-Pohjan',
			'Pirkanmaa': 'Pirkanmaan',
			'Pohjois-Karjala': 'Pohjois-Karjalan',
			'Pohjois-Pohjanmaa': 'Pohjois-Pohjanmaan',
			'Pohjois-Savo': 'Pohjois-Savon',
			'Päijät-Häme': 'Päijät-Hämeen',
			'Satakunta': 'Satakunnan',
			'Vaasa': 'Vaasan',
			'Varsinais-Suomi': 'Varsinais-Suomen'
		}
	};
}

CoronaModule.prototype.loadFile = function (filename, callback) {
	var that = this;
	fs.readFile(filename, function (err, data) {
		if(err) {
			console.log(err);
		} else {
			if(data.length > 0) {
				that.data = JSON.parse(data);
			}
			callback();
		}
	});
}

CoronaModule.prototype.saveFile = function (filename = this.filename, callback = function () {}) {
	fs.writeFile(filename, JSON.stringify(this.data), function (err) {
		if(err) {
			console.log(err);
		} else {
			if(global.debug) console.log(filename + ' saved');
			callback();
		}
	});
}

CoronaModule.prototype.init = function () {
	this.getUpdates();
	setInterval(this.getUpdates.bind(this), 1000 * 60);
}

CoronaModule.prototype.handleStart = function (command, options, chatID) {
	this.parent.sendMessage('Jos haluat päivityksiä Suomen koronatapauksista, kirjoita "/korona kyllä kiitos", ja jos haluat lopettaa tilauksen, kirjoita "/korona lopeta".\n\nTietolähteenä on HS:n avoin data: https://github.com/HS-Datadesk/koronavirus-avoindata', chatID);
}

CoronaModule.prototype.getUpdates = function () {
	var that = this;
	var httpsOptions = {
		hostname: 'w3qa5ydb4l.execute-api.eu-west-1.amazonaws.com',
		path: '/prod/finnishCoronaData'
	};
	
	var request = https.request(httpsOptions, function (response) {
		var responseData = '';
		response.on('data', function (data) {
			responseData += data;
		});
		response.on('end', function () {
			that.handleUpdate(JSON.parse(responseData));
		});
	});	
	request.end();
}

CoronaModule.prototype.handleUpdate = function (data) {
	// wtf atom and folding?
	for(var caseType of this.CASE_TYPES) {
		this.newData[caseType] = data[caseType];
		for(var infectionCase of this.newData[caseType]) {
			if(this.getCaseById(infectionCase.id, caseType, true) === false) {
				this.handleNewCase(caseType, infectionCase.id);
				continue;
			}
			
			if(this.checkCaseForChanges(caseType, infectionCase.id)) {
				this.handleUpdatedCase(caseType, infectionCase.id);
			}
		}
		this.data[caseType] = this.newData[caseType];
		this.newData[caseType] = [];
	}
	this.saveFile();
}

CoronaModule.prototype.handleCoronaCommand = function (command, options, chatID) {
	if(typeof options[0] == 'undefined') {
		this.handleConfusion(chatID);
		this.parent.sendMessage('Koko rimpsu samaan viestiin välilyönnillä eroteltuna, esimerkiksi näin:\n/korona kyllä kiitos', chatID);
		return;
	}
	
	for(var word of this.activationWords) {
		if(options[0].indexOf(word) > -1) {
			this.handleSubscribeMessage(chatID);
			return;
		}
	}
	
	for(var word of this.deactivationWords) {
		if(options[0].indexOf(word) > -1) {
			this.handleUnsubscribeMessage(chatID);
			return;
		}
	}
	
	this.handleConfusion(chatID);
	this.parent.sendMessage('Koko rimpsu samaan viestiin välilyönnillä eroteltuna, esimerkiksi näin:\n/korona kyllä kiitos', chatID);
}

CoronaModule.prototype.handleSubscribeMessage = function (chatID) {
	if(this.data.chats.indexOf(chatID) > -1) {
		this.handleConfusion(chatID);
		return false;
	}
	
	this.handleSubscribe(chatID);
}

CoronaModule.prototype.handleSubscribe = function (chatID) {
	this.data.chats.push(chatID);
	this.saveFile(this.filename);
	var message = this.subscribeMessages[Math.floor(Math.random() * this.subscribeMessages.length)];
	this.parent.sendMessage(message, chatID);
}

CoronaModule.prototype.handleUnsubscribeMessage = function (chatID) {
	if(this.data.chats.indexOf(chatID) == -1) {
		this.handleConfusion(chatID);
		return false;
	}
	
	this.handleUnsubscribe(chatID);
}

CoronaModule.prototype.handleUnsubscribe = function (chatID) {
	var index = this.data.chats.indexOf(chatID);
	this.data.chats.splice(index, 1);
	this.saveFile(this.filename);
	var message = this.unsubscribeMessages[Math.floor(Math.random() * this.unsubscribeMessages.length)];
	this.parent.sendMessage(message, chatID);
}

CoronaModule.prototype.handleConfusion = function (chatID) {
	var message = this.confusionMessages[Math.floor(Math.random() * this.confusionMessages.length)];
	this.parent.sendMessage(message, chatID);
}

CoronaModule.prototype.sendMessageToSubscribers = function (message) {
	for(var chatID of this.data.chats) {
		this.parent.sendMessage(message.trim(), chatID);
	}
}

CoronaModule.prototype.handleNewCase = function (caseType, id) {
	var infectionCase = this.getCaseById(id, caseType);
	var message = `${caseType === this.CASE_TYPE_CONFIRMED ? '🤒' : '💀'} Uusi vahvistettu ${caseType === this.CASE_TYPE_CONFIRMED ? 'koronatapaus' : 'koronakuolema'}:\nTapaus numero ${id} on todettu ${this.healthCareDistricts[this.GRAMMATICAL_CASE_GENITIVE][infectionCase.healthCareDistrict]} sairaanhoitopiirissä. ${this.getInfectionSourceString(id)}.\n`;

	this.sendMessageToSubscribers(message);
}

CoronaModule.prototype.handleUpdatedCase = function (caseType, id) {
	var knownData = this.getCaseById(id, caseType, true);
	var newData = this.getCaseById(id, caseType);
	
	var message = `Päivittynyt ${caseType === this.CASE_TYPE_CONFIRMED ? 'koronatapaus' : 'koronakuolema'}:\nTapaus ${id}\n`;
	var isChangeIdentified = false;
	if(knownData.healthCareDistrict !== newData.healthCareDistrict) {
		message += `- Sairaanhoitopiiri: ${newData.healthCareDistrict} (aiemmin ${knownData.healthCareDistrict}).\n`;
		isChangeIdentified = true;
	}
	
	if(this.getInfectionSourceString(id, true) !== this.getInfectionSourceString(id)) {
		message += `- Tartunnan lähde: ${this.getInfectionSourceString(id)} (aiemmin ${this.getInfectionSourceString(id, true)}).`;
		isChangeIdentified = true;
	}
	
	if(!isChangeIdentified) {
		message += 'Ei hajuakaan mikä on muuttunut.';
	}
	
	this.sendMessageToSubscribers(message);
}

CoronaModule.prototype.checkCaseForChanges = function (caseType, id) {
	var knownData = this.getCaseById(id, caseType, true);
	var newData = this.getCaseById(id, caseType);
	
	if(
		knownData.healthCareDistrict !== newData.healthCareDistrict ||
		knownData.infectionSourceCountry !== newData.infectionSourceCountry ||
		knownData.infectionSource !== newData.infectionSource
	) {
		return true;
	}
	
	return false;
}

CoronaModule.prototype.getInfectionSourceString = function (id, isKnown = false) {
	var infectionCase = this.getCaseById(id, undefined, isKnown);
	var infectionSourceString = 'Tartunnan lähde ei ole julkisesti tiedossa.';
	
	if(infectionCase.infectionSource == this.INFECTION_SOURCE_UNKOWN) {
		infectionSourceString = `Se on saatu ${this.getCountryByCode(infectionCase.infectionSourceCountry, this.GRAMMATICAL_CASE_ELATIVE)}, mutta tartuntaketju ei ole julkisesti tiedossa`;
	}
	
	if(infectionCase.infectionSource == this.INFECTION_SOURCE_RELATED) {
		infectionSourceString = `Se on saatu ${this.getCountryByCode(infectionCase.infectionSourceCountry, this.GRAMMATICAL_CASE_ELATIVE)} ja se liittyy johonkin aiempaan tapaukseen, mutta julkisesti ei ole tiedossa, mihin niistä`;
	}
	
	if(typeof infectionCase.infectionSource == 'number') {
		infectionSourceString = `Se on ${this.getCountryByCode(this.getFirstInfectionCountryCode(infectionCase.id), this.GRAMMATICAL_CASE_ELATIVE)} alkaneen tartuntaketjun ${this.getInfectionChainLength(infectionCase.id)}. polven tartunta`
	}
	
	return infectionSourceString;
}

CoronaModule.prototype.getCountryByCode = function (countryCode, grammaticalCase = this.GRAMMATICAL_CASE_NOMINATIVE) {
	if(typeof this.countries[grammaticalCase][countryCode] == 'undefined' && grammaticalCase == this.GRAMMATICAL_CASE_NOMINATIVE) {
		return '[Maa ei julkisesti tiedossa]';
	} else if(typeof this.countries[grammaticalCase][countryCode] == 'undefined' && grammaticalCase == this.GRAMMATICAL_CASE_ELATIVE) {
		return `maasta ${this.getCountryByCode(countryCode)}`;
	}
	
	return this.countries[grammaticalCase][countryCode];
}

CoronaModule.prototype.getFirstInfectionCountryCode = function (id) {
	var currentInfectionCase = this.getCaseById(id);
	if(typeof currentInfectionCase.infectionSource != 'number') {
		return currentInfectionCase.infectionSourceCountry;
	}
	
	return this.getFirstInfectionCountryCode(currentInfectionCase.infectionSource);
}

CoronaModule.prototype.getInfectionChainLength = function (id) {
	var currentInfectionCase = this.getCaseById(id);
	if(typeof currentInfectionCase.infectionSource != 'number') {
		return 1;
	}
	
	return this.getInfectionChainLength(currentInfectionCase.infectionSource) + 1;
}

CoronaModule.prototype.getCaseById = function (id, caseType = this.CASE_TYPE_CONFIRMED, isKnown = false) {
	var list = (isKnown ? this.data : this.newData)[caseType];
	for(var infectionCase of list) {
		if(infectionCase.id == id) {
			return infectionCase;
		}
	}
	return false;
}

module.exports = CoronaModule;
