function IltapaskaModule(parent) {
	this.parent = parent;
	this.parent.on('message', this.handleMessage, this);
}

IltapaskaModule.prototype.handleMessage = function (message) {
	if(message.text.indexOf('iltalehti.fi') != -1 || message.text.indexOf('iltasanomat.fi') != -1) {
		this.handleIltapaska(message);
	}
};

IltapaskaModule.prototype.handleIltapaska = function (message) {
	if(Math.random() < 0.1) {
		return;
	}
	
	var responseParts = [
		['nyt ', ''],
		['loppuu se ', 'lopetat sen ', 'saa loppua se ', 'saa loppua tuo '],
		['iltapaskan ', 'syövän ', 'paskan '],
		['postaaminen', 'levittäminen']
	];
	
	var responseText = '';
	
	responseParts.forEach(function (partGroup) {
		var part = partGroup[Math.floor(Math.random() * partGroup.length)];
		responseText += part;
	});
	
	if(Math.random() < 0.3) {
		responseText += ' ' + message.from.first_name.toLowerCase();
	}
	
	this.parent.sendMessage(responseText, message.chat.id);
}

module.exports = IltapaskaModule;
