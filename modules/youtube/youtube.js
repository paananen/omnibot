var fs = require('fs');

function YoutubeModule(parent) {
	this.parent = parent;
	this.parent.on('message', this.handleMessage, this);
	this.urlPrefix = 'https://youtube.com/watch?v=';
	this.filename = 'youtube_data';
	this.data = {};
	this.thanks = [
		'kiitos',
		'merci',
		'cheers',
		'tack',
		'gracias',
		'grazie',
		'thanks',
		'thank you',
		'danke',
		'mahalo',
		'спасибо',
		'شكرا'
	];
	var that = this;
	
	fs.readFile('modules/youtube/' + this.filename + '.json', function (err, data) {
		if(err) {
			console.log(err);
		} else {
			if(data.length == 0) {
				that.data = {
					videos: [],
					channels: []
				}
			} else {
				that.data = JSON.parse(data);
				console.log(that.data.videos.length + ' videos');
				console.log(that.data.channels.length + ' channels');
			}
		}
	});
}

YoutubeModule.prototype.handleMessage = function (message) {
	if(message.text.indexOf('/video') === 0) {
		this.handleVideoCommand(message);
	} else if(message.text.indexOf('/youtubestats') === 0) {
		this.handleStatsCommand(message);
	}
}

YoutubeModule.prototype.handleStatsCommand = function (message) {
	var responseText = this.data.videos.length + ' videos, ' + this.data.channels.length + ' channels';
	this.parent.sendMessage(responseText, message.chat.id);
}

YoutubeModule.prototype.handleVideoCommand = function (message) {
	var matches = message.text.match(/(?:youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9-_]{11})/);
	if(matches != null) {
		this.handleVideoRecommendation(matches[1], message);
	} else {
		this.handleVideoRequest(message);
	}
}

YoutubeModule.prototype.handleVideoRecommendation = function (videoID, message) {
	for(var i = 0; i < this.data.videos.length; i++) {
		if(this.data.videos[i].videoID == videoID) {
			this.handleDuplicate(message);
			return;
		}
	}
	this.data.videos.push({
		from: message.from,
		videoID: videoID
	});
	
	this.sendThankYou(message);
	
	this.saveData();
}

YoutubeModule.prototype.sendThankYou = function (message) {
	var i = Math.floor(Math.random() * this.thanks.length);
	this.parent.sendMessage(this.thanks[i] + ', ' + message.from.first_name.toLowerCase(), message.chat.id);
}

YoutubeModule.prototype.handleDuplicate = function (message) {
	this.parent.sendMessage('wanha', message.chat.id);
}

YoutubeModule.prototype.handleVideoRequest = function (message) {
	if(this.data.videos.length == 0) {
		this.parent.sendMessage('Opeta minua, senpai ;__;', message.chat.id);
		return;
	}
	var i = Math.floor(Math.random() * this.data.videos.length);
	this.parent.sendMessage(this.urlPrefix + this.data.videos[i].videoID, message.chat.id);
}

YoutubeModule.prototype.saveData = function () {
	var that = this;
	fs.writeFile('modules/youtube/' + this.filename + '.json', JSON.stringify(this.data), function (err) {
		if(err) {
			console.log(err);
		} else {
			console.log(that.filename + '.json saved');
		}
	});
}

module.exports = YoutubeModule;