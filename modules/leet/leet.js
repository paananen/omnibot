var fs = require('fs');

const MILLISECOND = 1;
const SECOND = 1000 * MILLISECOND;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;

function LeetModule(parent) {
	this.parent = parent;
	this.filename = 'leet_data.json';
	this.initScores();
	this.quietTimeThreshold = 6 * HOUR;
	this.failPenalty = 5;
	this.positionScores = [10, 5, 4, 3, 2, 1];
	this.successfulLeetsToday = {};
	this.loadFile(this.filename, this.init.bind(this));
	this.timeZone = 'Europe/Helsinki';
	this.dailyComboIncrement = 1;
	this.defaultTimeWindowLength = 60 * SECOND;
	this.defaultAllowedMissesCount = 0;
	this.defaultMaxComboBonus = 3;
	this.upperLimit = 13 * HOUR + 38 * MINUTE + 0 * SECOND;
	this.defaultUpgradeCostFunction = function (level) {
		return level * 10;
	};
	this.upgrades = {
		timeWindowExtension: {
			name: 'Time window extension',
			description: 'Makes your valid time window start n seconds earlier (where n = upgrade level).',
			upgradeCostFunction: this.defaultUpgradeCostFunction,
			upgradeMessageFunction: function (player, level) {
				return `Nice job, ${player.name}! You can now send your 1337 a whole ${level} seconds before 13:37:00.`;
			},
			disabled: true
		},
		maxComboBonus: {
			name: 'Maximum combo bonus',
			description: `Makes your maximum combo bonus ${this.defaultMaxComboBonus} + n (where n = upgrade level).`,
			upgradeCostFunction: this.defaultUpgradeCostFunction,
			upgradeMessageFunction: function (player, level) {
				return `Congratulations, ${player.name}! Your maximum streak bonus is now ${this.defaultMaxComboBonus + level} points.`;
			},
			disabled: false
		},
		allowedMisses: {
			name: 'Allowed misses',
			description: 'Allows you to miss n days without losing your combo (where n = upgrade level).',
			upgradeCostFunction: this.defaultUpgradeCostFunction,
			upgradeMessageFunction: function (player, level) {
				return `Money well spent, ${player.name}. Your can now miss your 1337 for ${level} ${level == 1 ? 'day' : 'days'} before losing your streak.`;
			},
			disabled: false
		},
		prestige: {
			name: 'Prestige',
			description: 'You\'ll get a crown emoji next to your name.',
			upgradeCostFunction: this.defaultUpgradeCostFunction,
			upgradeMessageFunction: function (player, level) {
				return `You're going to lose because of this, you must know that, ${player.name}? I guess you really like emoji.`;
			},
			disabled: false
		}
	}
}

LeetModule.prototype.loadFile = function (filename, callback) {
	var that = this;
	fs.readFile(filename, function (err, data) {
		if(err) {
			console.log(err);
		} else {
			if(data.length > 0) {
				that.data = JSON.parse(data);
			}
			callback();
		}
	});
}

LeetModule.prototype.saveFile = function (filename) {
	fs.writeFile(filename, JSON.stringify(this.data), function (err) {
		if(err) {
			console.log(err);
		} else {
			if(global.debug) console.log(filename + ' saved');
		}
	});
}

LeetModule.prototype.init = function () {
	this.parent.on('message', this.handleMessage, this);
	this.parent.registerCommand('scores', this.handleScoresCommand, this);
	this.parent.registerCommand('upgrades', this.handleUpgradesCommand, this);
	this.parent.registerCommand('upgrade', this.handleUpgradeCommand, this);
	setInterval(this.tick.bind(this), 10 * SECOND);
}

LeetModule.prototype.tick = function () {
	var date = new Date();
	var dateString = date.toLocaleString('fi-FI', {timeZone: this.timeZone});
	
	if(dateString.indexOf('13:38:1') > -1) {
		this.processPassedLeet();
	}
	this.saveFile(this.filename);
}

LeetModule.prototype.handleUpgradesCommand = function (command, options, chatId, message) {
	var chat = this.getChatData(chatId);
	var player = this.getPlayerData(message);
	
	var output = `<b>Upgrades for ${this.getPlayerName(player)}</b>`;
	output += `\nYou currently have ${player.upgradePoints} ProffaCoins`;
	for(var property in this.upgrades) {
		if(!this.upgrades.hasOwnProperty(property)) {
			continue;
		}
		var upgrade = this.upgrades[property];
		if(upgrade.disabled) {
			continue;
		}
		var upgradeLevel = this.getPlayerUpgradeLevel(player, property);
		var upgradeCost = upgrade.upgradeCostFunction(upgradeLevel + 1);
		output += `\n\n<b>${upgrade.name} (lvl ${upgradeLevel})</b>\n`;
		output += `<i>${upgrade.description}</i>\n`;
		output += `<code>/upgrade ${property}</code> (costs ${upgradeCost} ProffaCoins)`;
		if(upgradeCost > player.upgradePoints) {
			output += `\n<b>Can't afford</b>`;
		}
	}
	
	this.parent.sendMessage(output, chat.id);
}

LeetModule.prototype.handleUpgradeCommand = function (command, options, chatId, message) {
	var chat = this.getChatData(chatId);
	var player = this.getPlayerData(message);
	var upgradeId = options[0];
	
	if(typeof this.upgrades[upgradeId] == 'undefined') {
		this.parent.sendMessage('U wot m8?', chat.id);
		return;
	}
	
	var upgrade = this.upgrades[upgradeId];
	if(upgrade.disabled) {
		this.parent.sendMessage('Nah mate, not this time');
		return;
	}
	var currentLevel = this.getPlayerUpgradeLevel(player, upgradeId);
	var upgradeCost = upgrade.upgradeCostFunction(currentLevel + 1);
	
	if(player.upgradePoints < upgradeCost) {
		this.parent.sendMessage('Not enough ProffaCoins. Git gud.', chat.id);
		return;
	}
	
	player.upgradePoints -= upgradeCost;
	player.upgrades[upgradeId] = currentLevel + 1;
	this.parent.sendMessage('🎉', chat.id);
	this.parent.sendMessage(upgrade.upgradeMessageFunction.call(this, player, player.upgrades[upgradeId]), chat.id);
	this.parent.sendMessage(`You have ${player.upgradePoints} ProffaCoins remaining.`, chat.id);
}

LeetModule.prototype.processPassedLeet = function () {
	let isLastDay = this.isNowLastDayOfWeek();
	for(var chatId in this.data.chats) {
		if(this.data.chats.hasOwnProperty(chatId)) {
			var chat = this.data.chats[chatId];
			for(var playerId in chat.players) {
				if(chat.players.hasOwnProperty(playerId)) {
					var player = chat.players[playerId];
					if(typeof this.successfulLeetsToday[chat.id]  == 'undefined' || typeof this.successfulLeetsToday[chat.id][player.id] == 'undefined') {
						player.missedDays += 1;
						if(player.missedDays > this.getPlayerAllowedMissesCount(player)) {
							player.combo = 0;
						}
					}
				}
			}
			this.sendChatScores(chat, isLastDay);
		}
	}
	this.successfulLeetsToday = {};

	if(isLastDay) {
		this.initScores();
	}
}

LeetModule.prototype.sendChatScores = function (chat, isLastDay) {
	var output = '<b>Current scores:</b>';
	var players = [];
	for(var playerId in chat.players) {
		if(chat.players.hasOwnProperty(playerId)) {
			var player = chat.players[playerId];
			var playerData = this.getPlayerDataById(playerId, chat.id);
			players.push(playerData);
		}
	}
	
	if(players.length == 0) {
		this.parent.sendMessage('🦗', chat.id);
		return;
	}
	
	var sortedPlayers = players.sort(function (a, b) {
		return b.score - a.score
	});
	
	var i = 1;
	for(var player of players) {
		output += `\n\n<b>${i}. ${this.getPlayerName(player)}: ${player.score}</b>\nStreak: ${player.combo} (${this.getPlayerComboBonus(player)} pts)\nProffaCoins: ${player.upgradePoints}`;
		i++;
	}
	this.parent.sendMessage(output, chat.id);

	if(isLastDay) {
		let winner = sortedPlayers[0];
		this.parent.sendMessage(`Congratulations ${this.getPlayerName(winner)}, you won this week with ${player.score} points! Better luck to everyone else next week!`);
	}
}

LeetModule.prototype.initScores = function () {
	this.data = {
		chats: {}
	};
}

LeetModule.prototype.handleScoresCommand = function (command, options, chatId) {
	var chat = this.getChatData(chatId);
	this.sendChatScores(chat);
}

LeetModule.prototype.handleMessage = function (message) {
	if(message.text == '1337') {
		this.handleLeet(message);
	}
};

LeetModule.prototype.handleLeet = function (message) {
	var player = this.getPlayerData(message);
	
	var timestamp = message.date;
	var date = new Date(timestamp * SECOND);
	var timeOffset = this.getOffsetFromUpperLimit(date);
	var playerTimeWindowLength = this.getPlayerTimeWindowLength(player);
	
	if(timeOffset >= 0 && timeOffset < playerTimeWindowLength) {
		this.handleCorrectlyTimedLeet(player);
	} else {
		this.handleIncorrectlyTimedLeet(player);
	}
	player.latestLeet = Date.now();
}

LeetModule.prototype.getPlayerTimeWindowLength = function (player) {
	var playerTimeWindowExtensionLevel = this.getPlayerUpgradeLevel(player, 'timeWindowExtension');
	var playerTimeWindowLength = this.defaultTimeWindowLength + playerTimeWindowExtensionLevel;
	return playerTimeWindowLength;
}

LeetModule.prototype.getPlayerAllowedMissesCount = function (player) {
	var playerAllowedMissesLevel = this.getPlayerUpgradeLevel(player, 'allowedMisses');
	var playerAllowedMisses = this.defaultAllowedMissesCount + playerAllowedMissesLevel;
	return playerAllowedMisses;
}

LeetModule.prototype.getPlayerMaxComboBonus = function (player) {
	var playerMaxComboBonusLevel = this.getPlayerUpgradeLevel(player, 'maxComboBonus');
	var playerMaxComboBonus = this.defaultMaxComboBonus + playerMaxComboBonusLevel;
	return playerMaxComboBonus;
}

LeetModule.prototype.getPlayerComboBonus = function (player) {
	var playerMaxComboBonus = this.getPlayerMaxComboBonus(player);
	var comboBonus = Math.min(playerMaxComboBonus, player.combo);
	return comboBonus;
}

LeetModule.prototype.getPlayerUpgradeLevel = function (player, upgrade) {
	if(typeof player.upgrades[upgrade] == 'undefined') {
		return 0;
	}

	if(this.upgrades[upgrade].disabled) {
		return 0;
	}
	
	return player.upgrades[upgrade];
}

LeetModule.prototype.getPlayerName = function (player) {
	var playerPrestigeLevel = this.getPlayerUpgradeLevel(player, 'prestige');
	if(playerPrestigeLevel > 0) {
		return `${'👑'.repeat(playerPrestigeLevel)} ${player.name}`;
	}
	
	return player.name;
}

LeetModule.prototype.getOffsetFromUpperLimit = function (date) {
	var dateString = date.toLocaleString('fi-FI', {timeZone: this.timeZone});
	var timeString = dateString.split(' ')[1];
	var timeComponents = timeString.split(':');
	var hours = timeComponents[0];
	var minutes = timeComponents[1];
	var seconds = timeComponents[2];
	var time = hours * HOUR + minutes * MINUTE + seconds * SECOND;
	var dTime = this.upperLimit - time;
	return dTime;
}

LeetModule.prototype.handleCorrectlyTimedLeet = function (player) {
	if(Date.now() - player.latestLeet > this.quietTimeThreshold || player.latestLeet === null) {
		this.handleSuccessfulLeet(player);
	} else {
		this.handleFailedLeet(player);
	}
}

LeetModule.prototype.handleIncorrectlyTimedLeet = function (player) {
	this.handleFailedLeet(player);
}

LeetModule.prototype.handleSuccessfulLeet = function (player) {
	var successfulLeetsInChatToday = this.getSuccessfulLeetsInChatToday(player.chatId);
	var numberOfSuccessfulLeetsInChatToday = Object.keys(successfulLeetsInChatToday).length;
	
	if(numberOfSuccessfulLeetsInChatToday < this.positionScores.length) {
		let scoreGain = this.positionScores[numberOfSuccessfulLeetsInChatToday] + this.getPlayerComboBonus(player);
		player.score += scoreGain;
		player.upgradePoints += scoreGain;
	}
	successfulLeetsInChatToday[player.id] = true;
	player.combo += this.dailyComboIncrement;
	player.missedDays = 0;
}

LeetModule.prototype.handleFailedLeet = function (player) {
	player.score -= this.failPenalty;
	player.combo = 0;
}

LeetModule.prototype.initPlayerData = function (message) {
	var chatId = message.chat.id;
	var playerId = message.from.id;
	var name = message.from.first_name;
	var chat = this.getChatData(chatId);
	
	if(typeof chat.players[playerId] != 'undefined') {
		console.log('Player already exists');
		return;
	}
	
	chat.players[playerId] = {
		id: playerId,
		chatId: chatId,
		name: name,
		score: 0,
		upgradePoints: 0,
		combo: 0,
		latestLeet: null,
		upgrades: {},
		missedDays: 0
	};
}

LeetModule.prototype.initChatData = function (chatId) {
	if(typeof this.data.chats[chatId] != 'undefined') {
		console.log('Chat already exists');
		return;
	}
	
	this.data.chats[chatId] = {
		id: chatId,
		players: {}
	};
}

LeetModule.prototype.getChatData = function (chatId) {
	if(typeof this.data.chats[chatId] == 'undefined') {
		this.initChatData(chatId);
	}
	
	return this.data.chats[chatId];
}

LeetModule.prototype.getPlayerData = function (message) {
	var chatId = message.chat.id;
	var playerId = message.from.id;
	var chat = this.getChatData(chatId);
	
	if(typeof chat.players[playerId] == 'undefined') {
		this.initPlayerData(message);
	}
	
	var player = chat.players[playerId];
	
	if(typeof player.upgrades == 'undefined') {
		player.upgrades = {};
	}
	
	if(typeof player.missedDays == 'undefined') {
		player.missedDays = 0;
	}
	
	return player;
}

LeetModule.prototype.getPlayerDataById = function (playerId, chatId) {
	var fakeMessage = {
		chat: {
			id: chatId
		},
		from: {
			id: playerId
		}
	};
	
	return this.getPlayerData(fakeMessage);
}

LeetModule.prototype.getSuccessfulLeetsInChatToday = function (chatId) {
	if(typeof this.successfulLeetsToday[chatId] == 'undefined') {
		this.successfulLeetsToday[chatId] = {};
	}
	
	return this.successfulLeetsToday[chatId];
}

LeetModule.prototype.isNowLastDayOfMonth = function () {
	let currentDate = new Date();
	let nextDay = new Date(currentDate.getTime() + 24 * HOUR);
	return nextDay.getDate() == 1;
}

LeetModule.prototype.isNowLastDayOfWeek = function () {
	let currentDate = new Date();
	return currentDate.getDay() == 0;
}

module.exports = LeetModule;