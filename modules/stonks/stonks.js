var fs = require('fs');
const https = require('https');

function StonksModule(parent) {
	this.parent = parent;
	this.phrases = {
		positive: [
			'kurssi on vuodessa noussut jo $0, ja',
			'osake on raketoinut tänä vuonna $0, ja'
		],
		negative: [
			'vaikka laskua onkin tullut vuodessa $0, niin',
			'osakkeen hintaan on tänä vuonna tullut alennusta $0, ja',
			'kurssia on korjattu tänä vuonna $0 alaspäin, ja'
		],
		intro: [
			'Tämänhetkinen suositus on $0,',
			'Tällä hetkellä $0 on varma valinta,',
			'Vaikea valinta, mutta sanoisin $0,',
			'Ei tarvitse edes miettiä, ainoa oikea vastaus on $0,',
			'Kyllä nyt tyhmempikin botti osaa poimia pellosta sen mansikan, eli $0,',
			'Huhujen mukaan $0 -raketin tankkaaminen on jo aloitettu,'
		],
		excuse: {
			'NOK' : [
				'kaikki tietää, että ne rakentaa kohta 4G-verkon kuuhun!',
				'Huawei tullaan kieltämään lailla, ja kaikki 5G-verkot tulee Nokialta.',
				'Nokian 5G-verkkolaitteet on markkinoiden eettisimpiä.',
				'on se vaan hieno yhtiö. Uskon edelleen, että se nousee.',
				'syyskuussa 2020 Nokia kertoi digitalisoineensa kaikki 5G-verkkonsa!',
				'se on nyt halpaa, ja silloin pitää ostaa.',
				'osta sitä vitun dippiä nyt vaan niinku isot pojat.',
				'on se aina ennenkin noussu.',
				'kyllähän suomalaisen pitää Nokiaa omistaa.',
				'ajattele 10 vuoden päähän, ei isoja rahoja viikossa tehdä.',
				'se on iso pulju, ei se mene nurin.',
				'jos joku yksikkö ei pärjää niin se kyllä myydään pois.',
				'uusi toimari kyllä luotsaa Nokian maaliin.',
				'ongelmat on tällä kertaa varmasti tilapäisiä.',
				'korjausliike ylöspäin tulee varmasti.',
				'ostat nyt, ja sitten seuraavan romahduksen jälkeen lisää.',
				'Nokia on oikea yhtiö, sen liiketoiminta on ymmärrettävää ja bisnes kohtuullisen vakaata.',
				'sijoitusmielessä nämä ovat oivia paikkoja tehdä pikavoittoja.',
				'Nokialla ei ole enää paljoa laskuvaraa, joten nyt on hyvä hetki ostaa.'
			]
		}
	};
	var that = this;
	fs.readFile('modules/stonks/iex-cloud-token.txt', 'utf8', function (err, data) {
		if(err) {
			console.log(err);
		} else {
			that.token = data.trim();;
			that.httpsOptions = {
				hostname: 'cloud.iexapis.com',
				path: `/stable/stock/nok/quote?token=${that.token}`
			};
		}
		that.init();
	});
}

StonksModule.prototype.init = function () {
	this.parent.registerCommand('stonks', this.handleStonksCommand, this);
}

StonksModule.prototype.handleStonksCommand = function (command, options, chatID) {
	var that = this;
	var request = https.request(this.httpsOptions, function (response) {
		response.on("data", function (data) {
			var quote = JSON.parse(data);
			that.parent.sendMessage(that.getRecommendationMessage(quote), chatID);
		});
	});
	request.end();
}

StonksModule.prototype.getRecommendationMessage = function (quote) {
	var change = `${Math.round(Math.abs(quote.ytdChange) * 10000) / 100} %`;
	
	var message = [];
	message.push(this.getRandomPhrase(this.phrases.intro, [`<b>${quote.companyName} (${quote.symbol})</b>`]));
	message.push(quote.ytdChange < 0 ? this.getRandomPhrase(this.phrases.negative, [change]) : this.getRandomPhrase(this.phrases.positive, [change]));
	message.push(this.getRandomPhrase(this.phrases.excuse[quote.symbol]));
	
	return message.join(' ');
}

StonksModule.prototype.getRandomPhrase = function (list, values = []) {
	var index = Math.floor(Math.random() * list.length);
	var phrase = list[index];
	for(var i = 0; i < values.length; i++) {
		phrase = phrase.replace(`$${i}`, values[i]);
	}
	return phrase;
}

module.exports = StonksModule;