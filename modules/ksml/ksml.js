const https = require('https');

function KsmlModule(parent) {
	this.parent = parent;
	this.init();
}

KsmlModule.prototype.init = function () {
	this.parent.on('message', this.handleMessage, this);
}

KsmlModule.prototype.handleMessage = function (message) {
	if(message.text.indexOf('ksml.fi') > -1 && message.text.indexOf('pwbi=') == -1) {
		this.handleKsmlArticle(message);
	}
}

KsmlModule.prototype.handleKsmlArticle = function (message) {
	var path = message.text.match(/.*ksml\.fi([^\s#]*)/)[1];
	if(!path) {
		console.log(`Couldn't find ksml article path in ${message.text}`)
	}

	var requestOptions = {
		hostname: 'www.ksml.fi',
		path: path
	};

	var that = this;
	var request = https.request(requestOptions, function (response) {
		var responseData = '';
		response.on('data', function (data) {
			responseData += data;
		});
		response.on('end', function () {
			var match = responseData.match(/https?:\/\/[^\s]*pwbi=[0-9a-f]+/);
			if(!match) {
				that.parent.sendMessage('Onko ne perkele korjannu maksumuurinsa...', message.chat.id);
				return;
			}
			that.parent.sendMessage(`<a href="${match[0]}">Ohita maksumuuri</a>`, message.chat.id, true);
		});
	});	
	request.end();
}

module.exports = KsmlModule;
