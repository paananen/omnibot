const {OpenAI} = require('openai');
const fs = require('fs');
let openai;

function Proffa3Module(parent) {
	this.parent = parent;
	this.assistantId = 'asst_KA2J2gmCUbGInJSY7VjiebAi';
	this.threadId;
	this.threadTimestamp;
	this.threadMaxLength = 15;
	this.newThreadLength = 5;
	this.threadMessages = [];
	this.whitelistUserIds = [29893992,30200302,30172702,43572509,157174085,64272208];
	this.whitelistChatIds = [29893992, -2794222];
	this.messageQueue = [];
	this.incompleteRuns = [];
	this.messageQueueMinInterval = 1500;
	this.messageQueueTimer;
	this.threadMaxLife = 300000;

	var that = this;
	fs.readFile('openai-token.txt', 'utf8', function (err, data) {
		if(err) {
			console.log(err);
		} else {
			that.apiKey = data.trim();
		}
		that.init();
	});
}

Proffa3Module.prototype.init = function () {
	openai = new OpenAI({apiKey: this.apiKey});
	this.parent.on('message', this.handleMessage, this);
	setTimeout(this.sendMessageFromQueue.bind(this), this.messageQueueMinInterval);
};

Proffa3Module.prototype.handleMessage = async function (message) {
//	console.log('new message', this.messageQueue.length);
	if(this.whitelistChatIds.indexOf(message.chat.id) == -1) return;
	if(this.whitelistUserIds.indexOf(message.from.id) == -1) return; // not whitelisted user
/*	const thread = await openai.beta.threads.create({
		messages: [
			{
				role: 'user',
				content: `${(new Date()).toLocaleString('fi-FI')} käyttäjä ${message.from.first_name} sanoi: ${message.text}`
			}
		]
	});*/
	/*await openai.beta.threads.messages.create(
		this.threadId, {
			role: 'user',
			content: `${(new Date()).toLocaleString('fi-FI')} käyttäjä ${message.from.first_name} sanoi: ${message.text}`
		}
	);*/

	this.messageQueue.push(message);
};

Proffa3Module.prototype.sendMessage = function (messageText, chatId, responseMessageId) {
	this.parent.sendMessage(messageText, chatId, null, responseMessageId);
};

Proffa3Module.prototype.addMessageToMessageHistory = function (messageText, senderName, timestamp) {
	let message = {
		messageText,
		senderName,
		timestamp
	};
	this.threadMessages.unshift(message);
};

Proffa3Module.prototype.sendMessageFromQueue = async function () {
	//console.log('messages in queue: ', this.messageQueue.length);
	//console.log('runs in progress: ', this.incompleteRuns);

	if(this.incompleteRuns.length > 0 || this.messageQueue.length == 0) {
		setTimeout(this.sendMessageFromQueue.bind(this), this.messageQueueMinInterval);
		return;
	}

	let message = this.messageQueue[0];

	this.threadMessages.unshift({
		messageText: message.text,
		senderName: message.from.first_name,
		timestamp: new Date(message.date * 1000)
	});

	if(global.debug) console.log('threadId: ', this.threadId);
	if(global.debug) console.log('thread length: ', this.threadMessages.length);

	if(typeof this.threadId == 'undefined' || this.threadMessages.length >= this.threadMaxLength) {
		await this.createNewThread();
	}

	
	let needsResponse = false;
	if(message.text.toLowerCase().indexOf('proffa') > -1) {
		needsResponse = true;
	}
	let timestamp = (new Date()).toLocaleString('fi-FI', {timeZone: 'Europe/Helsinki'});
	await openai.beta.threads.messages.create(
		this.threadId, {
				role: 'user',
				content: `${timestamp} ${message.from.first_name}: ${message.text}`
			}
		);
	if(this.incompleteRuns.length > 0) return;
	if(needsResponse) await this.respondToMessage(message.chat.id, message.message_id);
	this.messageQueue.splice(0, 1);
	setTimeout(this.sendMessageFromQueue.bind(this), this.messageQueueMinInterval);
};

Proffa3Module.prototype.createNewThread = async function () {
	if(global.debug) console.log('creating thread');
	this.threadMessages.splice(this.newThreadLength);
	let messageHistory = this.threadMessages.reverse().map(message => {
		let timestamp = message.timestamp.toLocaleString('fi-FI', {timeZone: 'Europe/Helsinki'});
		return {
			role: 'user',
			content: `${timestamp} ${message.senderName}: ${message.messageText}`
		};
	});
	if(global.debug) console.log(messageHistory);
	let thread = await openai.beta.threads.create({
		messages: messageHistory
	});
	this.threadId = thread.id;
	if(global.debug) console.log('thread created: ', this.threadId);
};

Proffa3Module.prototype.respondToMessage = async function (chatId, messageId) {
	this.parent.setTyping(chatId);
	const run = await openai.beta.threads.runs.create(this.threadId, {assistant_id: this.assistantId});
	this.incompleteRuns.push(run.id);
	let that = this;
	let queueChecker = setInterval(async function () {
			let checkedRun = await openai.beta.threads.runs.retrieve(that.threadId, run.id);
			if(global.debug) console.log(checkedRun)
			if(checkedRun.status == 'completed') {
					clearInterval(queueChecker);
					that.incompleteRuns.splice(that.incompleteRuns.indexOf(checkedRun.id), 1);
					let messages = await openai.beta.threads.messages.list(that.threadId);
					let messageText = messages.data[0].content[0].text.value;
					that.sendMessage(messageText, chatId, messageId);
					that.addMessageToMessageHistory(messageText, 'Proffa', new Date());
			}
	}, 2000);
};

module.exports = Proffa3Module;