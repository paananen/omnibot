var http = require("http");
var TelegramBot = require('../TelegramBot/TelegramBot');
var fs = require('fs');
var bot = new OmniBot();

fs.readFile('debug-mode-allowed-recipients.json', function (err, data) {
	if(err) {
		console.log(err);
	} else {
		bot.debugModeAllowedRecipients = JSON.parse(data);
	}
});

fs.readFile('telegram-token.txt', function (err, data) {
	if(err) {
		console.log(err);
	} else {
		bot.token = data;
		bot.init();
	}
});

function OmniBot() {
	this.moduleNames = ['leet', 'proffa3'];
	this.modules = {};
	this.commandHandlers = [];
	this.eventHandlers = {
		'message': []
	};
	this.debugModeAllowedRecipients = [];
}

OmniBot.prototype.init = function () {
	this.telegram = new TelegramBot(this.token);
	// var that = this;
	// fs.readdir('modules', function (err, files) {
	// 	if(err) {
	// 		console.log(err);
	// 	} else {
	// 		that.moduleNames = files;
	// 		that.initModules(that.initDone.bind(that));
	// 	}
	// });
	this.initModules(this.initDone.bind(this));
}

OmniBot.prototype.initModules = function (callback) {
	var that = this;
	this.moduleNames.forEach(function (moduleName) {
		var module = require('./modules/' + moduleName + '/' + moduleName);
		that.modules[moduleName] = new module(bot);
	});
	callback();
}

OmniBot.prototype.initDone = function () {
	setInterval(this.getUpdates.bind(this), 5000);
}

OmniBot.prototype.getUpdates = function () {
	this.telegram.getUpdates(this.handleUpdates.bind(this));
}

OmniBot.prototype.handleUpdates = function (updates) {
	var that = this;
	updates.forEach(function (update) {
		// console.log(update);
		if(typeof update.message == 'undefined') {
			return false;
		}
		
		if(typeof update.message.text == 'undefined') {
			return false;
		}
		
		that.eventHandlers.message.forEach(function (handler) {
			handler(update.message);
		});
		
		var components = update.message.text.match(/^\/([a-zA-ZåäöÅÄÖ]+)(?:\s+(.+))?/);
		if(global.debug) console.log(update.message.text);
		if(components !== null) {
			if(global.debug) console.log(components);
			if(typeof components[2] == 'string') {
				var options = components[2].split(',').map(function (option) { return option.trim(); });
			} else {
				var options = [];
			}
			that.commandHandlers.forEach(function (commandHandler) {
				if(commandHandler.command == components[1]) {
					commandHandler.handler(components[1], options, update.message.chat.id, update.message);
				}
			});
		}
	});
}

OmniBot.prototype.WebRequest = function (hostname, path, callback) 
{
	var httpOptions = {
		hostname : hostname,
		path : path
	};
	
	var request = http.request(httpOptions, function (response) {
		response.on("data", function (data) {
			callback(data.toString("utf8"));
		});
	});
	
	request.end();
}

OmniBot.prototype.sendMessage = function (messageText, chatID, disableWebPagePreview, responseMessageId) {
	if(global.debug && this.debugModeAllowedRecipients.indexOf(chatID) == -1) {
		console.log('Not sending message in debug mode');
		return false;
	}
	this.telegram.sendMessage(messageText, chatID, disableWebPagePreview, responseMessageId);
}

OmniBot.prototype.setTyping = function (chatId) {
	this.telegram.setTyping(chatId);
}

OmniBot.prototype.registerCommand = function (command, handler, context) {
	this.commandHandlers.push({
		'command': command,
		'handler': handler.bind(context)
	});
}

OmniBot.prototype.on = function (eventName, handler, context) {
	this.eventHandlers[eventName].push(handler.bind(context));
}
